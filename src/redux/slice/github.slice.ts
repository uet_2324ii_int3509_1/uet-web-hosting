import { Status } from '~/enum/app.enum.ts';
import { createSlice } from '@reduxjs/toolkit';
import { loadAccountInfo, loadAllProjects } from '~/redux/actions/gitlab.action';

export interface GithubSliceState {
  isConnected: boolean;
  account: {
    data: any;
    status: Status;
  };
  projects: {
    data: any[];
    status: Status;
  };
}

export const initialGithubState: GithubSliceState = {
  isConnected: false,
  account: {
    data: {} as any,
    status: Status.IDLE
  },
  projects: {
    data: [],
    status: Status.IDLE
  }
};

export const githubSlice = createSlice({
  name: 'github',
  initialState: initialGithubState,
  reducers: {
    doRevoke: (state) => {
      state.isConnected = false;
      state.account = {
        data: {} as any,
        status: Status.IDLE
      };
    },
    setIsConnected: (state, action) => {
      state.isConnected = action.payload.isConnected;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadAccountInfo.pending, (state) => {
        state.account.status = Status.PENDING;
      })
      .addCase(loadAccountInfo.fulfilled, (state, action) => {
        state.account.data = action.payload.data.account;
        state.account.status = Status.FULFILLED;
      })
      .addCase(loadAccountInfo.rejected, (state) => {
        state.account.status = Status.REJECTED;
      });

    builder
      .addCase(loadAllProjects.pending, (state) => {
        state.projects.status = Status.PENDING;
      })
      .addCase(loadAllProjects.fulfilled, (state, action) => {
        state.projects.data = action.payload.data.projects;
        state.projects.status = Status.FULFILLED;
      })
      .addCase(loadAllProjects.rejected, (state) => {
        state.projects.status = Status.REJECTED;
      });
  }
});

export const { setIsConnected, doRevoke } = githubSlice.actions;
