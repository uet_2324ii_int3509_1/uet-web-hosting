import { createAsyncThunk } from '@reduxjs/toolkit';
import { githubService } from '~/services/github.service.ts';

export const loadGithubAccountInfo = createAsyncThunk<any>('gitlab/load-account-info', async () => {
  return (await githubService.getAccountInfo())?.data;
});

export const loadAllGithubProjects = createAsyncThunk<any>('gitlab/load-all-projects', async () => {
  return (await githubService.getAllProjects())?.data;
});
