import { Log } from '~/types/app.type.ts';
import styles from './style.module.scss';

function LogLine(props: Log) {
  return (
    <div className={`flex gap-4 items-start`}>
      <div className={styles.time}>{new Date(props?._source?.createdAt).toLocaleTimeString()}</div>
      <div>
        {`[${props?._source?.status}]`} {props?._source?.log.split('#').map((line) => <div>{`#${line}`}</div>)}
      </div>
    </div>
  );
}

export default LogLine;
