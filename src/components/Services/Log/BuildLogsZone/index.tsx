import styles from '../style.module.scss';
import { Skeleton, Typography } from 'antd';
import usePromise from '~/hooks/usePromise.ts';
import { servicesService } from '~/services/services.service.ts';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { Status } from '~/enum/app.enum.ts';
import LogLine from '~/components/Services/Log/LogLine';

let firstLoad = true;

function BuildLogsZone() {
  const [{ status, data }, doLoadLogs] = usePromise(servicesService.getLogs);
  const params = useParams();
  const buildLogs = data?.data?.data?.buildLogs?.logs;

  useEffect(() => {
    const timer = setInterval(() => {
      doLoadLogs(params?.id).finally(() => {
        firstLoad = false;
      });
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  console.log({ buildLogs });

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <div>
          <Typography.Title className={'mb-0'} level={5}>
            Build logs
          </Typography.Title>
        </div>
      </div>
      <div className={styles.content}>
        {status === Status.FULFILLED || !firstLoad ? (
          Array.isArray(buildLogs) && buildLogs.map((log) => <LogLine {...log} />)
        ) : (
          <Skeleton />
        )}
      </div>
    </div>
  );
}

export default BuildLogsZone;
