import { DeployLog } from '~/types/app.type.ts';
import styles from '~/components/Services/Log/LogLine/style.module.scss';

function DeployLogLine(props: DeployLog) {
  return (
    <div className={`flex gap-4 items-start`}>
      <div className={styles.time}>{new Date(props?._source?.['@timestamp']).toLocaleTimeString()}</div>
      <div>{props?._source?.log.split('#').map((line) => <div>{`#${line}`}</div>)}</div>
    </div>
  );
}

export default DeployLogLine;
