import styles from '~/components/Services/Log/style.module.scss';
import { Skeleton, Typography } from 'antd';
import usePromise from '~/hooks/usePromise.ts';
import { servicesService } from '~/services/services.service.ts';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { Status } from '~/enum/app.enum.ts';
import DeployLogLine from '~/components/Services/Log/DeployLogsZone/DeployLogLine';

let firstLoad = true;

function DeployLogsZone() {
  const [{ data }, doLoadLogs] = usePromise(servicesService.getLogs);
  const params = useParams();
  const deployLogs = data?.data?.data?.deployLogs?.logs;

  useEffect(() => {
    console.log("Run");
    const timer = setInterval(() => {
      doLoadLogs(params?.id).finally(() => {
        firstLoad = false;
      });
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  console.log({ deployLogs });
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <div>
          <Typography.Title className={'mb-0'} level={5}>
            Runtime logs
          </Typography.Title>
        </div>
      </div>
      <div className={styles.content}>
        {status === Status.FULFILLED || !firstLoad ? (
          Array.isArray(deployLogs) && deployLogs.map((log) => <DeployLogLine {...log} />)
        ) : (
          <Skeleton />
        )}
      </div>
    </div>
  );
}

export default DeployLogsZone;
