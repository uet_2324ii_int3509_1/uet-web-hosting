import { Button } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGithub, } from '@fortawesome/free-brands-svg-icons';
import { GetAuthUrlOptions, } from '~/services/gitlab.service.ts';
import {
  GITHUB_REDIRECT_URI,
  VITE_GITHUB_CLIENT_ID,
} from '~/config/gitlab.config.ts';
import { GitStateAction, Status } from '~/enum/app.enum.ts';
import { useAppSelector } from '~/redux/store';
import { githubService } from '~/services/github.service.ts';

function ConnectGithubButton() {
  const isConnected = useAppSelector((state) => state.github.isConnected);
  const loadingAccountStatus = useAppSelector((state) => state.github.account.status);
  return (
    <Button
      loading={loadingAccountStatus === Status.PENDING}
      danger={isConnected}
      block
      icon={<FontAwesomeIcon icon={faGithub} />}
      onClick={() => {
        if (!isConnected) {
          // Handle connect to github
          const options: GetAuthUrlOptions = {
            client_id: VITE_GITHUB_CLIENT_ID,
            redirect_uri: GITHUB_REDIRECT_URI,
            scope: 'read_user api write_repository read_repository',
            state: {
              action: GitStateAction.CONNECT,
              redirect_path: '/web/select-repo'
            }
          };
          window.location.href = githubService.getAuthorizationUrl(options);
        }
      }}
    >
      {`${isConnected ? 'Disconnect' : 'Connect'}`} to Github
    </Button>
  );
}

export default ConnectGithubButton;
