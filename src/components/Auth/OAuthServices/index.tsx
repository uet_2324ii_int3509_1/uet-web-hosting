import { Form } from 'antd';
import LoginGitlabButton from '~/components/Auth/OAuthServices/LoginGitlabButton';
import LoginGithubButton from '~/components/Auth/OAuthServices/LoginGithubButton.tsx';

function OAuthServices() {
  return (
    <div>
      <Form.Item>
        <LoginGithubButton />
      </Form.Item>
      <Form.Item>
        <LoginGitlabButton />
      </Form.Item>
    </div>
  );
}

export default OAuthServices;
