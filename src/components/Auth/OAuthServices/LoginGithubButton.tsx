import { Button } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGithub } from '@fortawesome/free-brands-svg-icons';
import { GetAuthUrlOptions } from '~/services/gitlab.service';
import { VITE_GITHUB_CLIENT_ID, GITHUB_REDIRECT_URI } from '~/config/gitlab.config';
import { GitStateAction } from '~/enum/app.enum';
import { githubService } from '~/services/github.service.ts';

function LoginGitlabButton() {
  return (
    <Button
      block
      icon={<FontAwesomeIcon icon={faGithub} />}
      onClick={() => {
        const options: GetAuthUrlOptions = {
          client_id: VITE_GITHUB_CLIENT_ID,
          redirect_uri: GITHUB_REDIRECT_URI,
          scope: 'repo user',
          state: {
            action: GitStateAction.AUTH
          }
        };
        window.location.href = githubService.getAuthorizationUrl(options);
      }}
    >
      Continue with Github
    </Button>
  );
}

export default LoginGitlabButton;
