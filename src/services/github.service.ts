import { jwtService } from '~/services/jwt.service.ts';
import { GetAuthUrlOptions } from '~/services/gitlab.service.ts';
import { httpService } from '~/services/http.service.ts';
import { GitlabAccountReturned, GitlabProjectReturned } from '~/types/gitlab.type.ts';

interface ConnectPayload {
  code: string;
  redirect_uri: string;
}

class GithubService {
  static END_POINT: string = 'https://github.com';

  public getAuthorizationUrl(options: GetAuthUrlOptions): string {
    const url = new URL(GithubService.END_POINT);
    url.pathname = '/login/oauth/authorize';
    url.searchParams.append('response_type', options?.response_type || 'code');
    url.searchParams.append('client_id', options?.client_id);
    url.searchParams.append('redirect_uri', options?.redirect_uri);

    if (options?.scope) {
      url.searchParams.append('scope', options?.scope);
    }

    if (options?.state) {
      url.searchParams.append('state', jwtService.encode(options?.state));
    }
    return url.toString();
  }

  public getAccountInfo = async () => {
    return await httpService.get<GitlabAccountReturned>('/github/user-info');
  };

  public getAllProjects = async () => {
    return await httpService.get<GitlabProjectReturned>(`/github/projects`);
  };

  public connect = async (payload: ConnectPayload) => {
    return await httpService.get('/github/connect', payload);
  };
}

export const githubService = new GithubService();