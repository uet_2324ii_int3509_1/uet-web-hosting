import { Alert, Spin } from 'antd';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useEffect } from 'react';
import { jwtService } from '~/services/jwt.service';
import { useSnackbar } from 'notistack';
import { GitStateAction, Token } from '~/enum/app.enum';
import { GITHUB_REDIRECT_URI } from '~/config/gitlab.config.ts';
import { useAppDispatch } from '~/redux/store';
import { setIsConnected } from '~/redux/slice/github.slice';
import { authService } from '~/services/auth.service.ts';
import StorageService from '~/services/storage.service.ts';
import { githubService } from '~/services/github.service.ts';
import { GetAuthUrlOptions } from '~/services/gitlab.service.ts';

function AuthGithubCallback() {
  const [searchParams] = useSearchParams();
  const code = searchParams.get('code');
  const state = searchParams.get('state');
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  useEffect(() => {
    try {
      if (!code || !state) {
        throw new Error('Action to verify is not valid!');
      }
      const decodedState: GetAuthUrlOptions['state'] = jwtService.decode(state!);
      const actionType = decodedState?.action;
      const redirectPath = decodedState?.redirect_path;

      switch (actionType) {
        case GitStateAction.CONNECT: {
          githubService
            .connect({ code, redirect_uri: GITHUB_REDIRECT_URI })
            .then((response) => {
              console.log({ response });
              dispatch(setIsConnected({ isConnected: true }));
              enqueueSnackbar('Connect to github successfully!', { variant: 'success' });
            })
            .catch(() => {
              dispatch(setIsConnected({ isConnected: false }));
              enqueueSnackbar('Failed to connect to github!', { variant: 'error' });
            })
            .finally(() => {
              navigate(redirectPath);
            });
          break;
        }
        case GitStateAction.AUTH: {
          authService
            .loginWithSSO({
              redirectUri: GITHUB_REDIRECT_URI,
              authorizationCode: code,
              service: 'github'
            })
            .then((response) => {
              StorageService.set(Token.ACCESS, response?.data?.access_token);
              StorageService.set(Token.REFRESH, response?.data?.refresh_token);
              navigate('/');
            });
          break;
        }
      }
    } catch {
      enqueueSnackbar('Action to verify is not valid!', { variant: 'error' });
    }
  }, []);

  return (
    <Spin>
      <Alert description='Just a second, we are verifying your github authentication.' type='info' />
    </Spin>
  );
}

export default AuthGithubCallback;
