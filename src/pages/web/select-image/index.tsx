import { Button, Card, Col, Collapse, Divider, Form, Input, Layout, message, Row, Typography } from 'antd';
import NewFieldItem from '~/components/Services/Web/NewFieldItem';
import { faArrowRight, faCube } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CreateDockerhubAuthenticationPayload } from '~/types/service.type.ts';
import { servicesService } from '~/services/services.service.ts';
import { useState } from 'react';
import { Status } from '~/enum/app.enum.ts';
import { useNavigate } from 'react-router-dom';

function SelectImagePage() {
  const [status, setStatus] = useState(Status.IDLE);
  const [form] = Form.useForm();
  const navigate = useNavigate();

  async function onVerifyImage(payload: any) {
    try {
      setStatus(Status.PENDING);
      const createAuthPayload: CreateDockerhubAuthenticationPayload = {} as CreateDockerhubAuthenticationPayload;
      if (payload?.name) {
        createAuthPayload.name = payload?.name;
      }
      if (payload?.username) {
        createAuthPayload.username = payload?.username;
      }
      if (payload?.personalAccessToken) {
        createAuthPayload.personalAccessToken = payload?.personalAccessToken;
      }

      let dockerHubAuthId = '';

      const dockerAuthResponse: any = await servicesService.getDockerhubAuthentication();
      const listDockerAuth = dockerAuthResponse?.data;
      if (Array.isArray(listDockerAuth) && listDockerAuth.length) {
        dockerHubAuthId = listDockerAuth[0]?._id;
      } else {
        if (Object.keys(createAuthPayload).length !== 0) {
          const createdDockerAuthResponse: any = await servicesService.createDockerhubAuthentication(createAuthPayload);
          dockerHubAuthId = createdDockerAuthResponse?.data?._id;
        }
      }

      const [repoName, tagName] = payload.registryUrl.split(':');
      console.log({ repoName, tagName });
      const response: any = await servicesService.checkIsExistImage({
        repoName,
        tagName,
        dockerHubAuthId
      });

      if (response?.data?.isExist) {
        message.success('Verify image successfully!');
        navigate(`/web/new-image?registryUrl=${payload?.registryUrl}&dockerhubAuthId=${dockerHubAuthId}`);
      } else {
        message.error('Failed to verify your image!');
      }
      setStatus(Status.FULFILLED);
    } catch (error) {
      message.error('Failed to verify your image!');
      setStatus(Status.REJECTED);
    }
  }

  return (
    <Layout
      style={{
        maxWidth: 992,
        margin: '0 auto'
      }}
    >
      <div className={'mb-12'}>
        <Typography.Title level={3}>Create a new Web Service</Typography.Title>
        <Typography.Text type={'secondary'}>
          Deploy your web service from an image hosted on a public or private registry.
        </Typography.Text>
      </div>
      <Card bordered={false}>
        <Form form={form} onFinish={onVerifyImage}>
          <Row gutter={[32, 24]}>
            <Col span={24}>
              <Typography.Title className={'mb-0'} level={5}>
                Deploy an image
              </Typography.Title>
            </Col>
            <Col span={8}>
              <NewFieldItem title={'Image URL'} description={'The image URL for your external image.'} />
            </Col>
            <Col span={16}>
              <Form.Item name={'registryUrl'} rules={[{ required: true }]}>
                <Input prefix={<FontAwesomeIcon icon={faCube} />} placeholder={'docker.io/library/nginx:latest'} />
              </Form.Item>
            </Col>
            <Divider className={'my-0'} />
            <Collapse
              className={'w-full'}
              defaultActiveKey={[]}
              ghost
              items={[
                {
                  key: 'credentials',
                  label: 'Credentials (Optional)',
                  children: (
                    <Row gutter={[32, 24]}>
                      <Col span={8}>
                        <NewFieldItem title={'Credential name'} description={'Enter your credential name.'} />
                      </Col>
                      <Col span={16}>
                        <Form.Item name={'name'}>
                          <Input placeholder={'Credential name...'} />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <NewFieldItem title={'Username'} description={'Your credential username.'} />
                      </Col>
                      <Col span={16}>
                        <Form.Item name={'username'}>
                          <Input placeholder={'Username...'} />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <NewFieldItem
                          title={'Personal Access Token'}
                          description={'Your credential personal access token.'}
                        />
                      </Col>
                      <Col span={16}>
                        <Form.Item name={'personalAccessToken'}>
                          <Input placeholder={'Personal access token...'} />
                        </Form.Item>
                      </Col>
                    </Row>
                  )
                }
              ]}
            />

            <Divider className={'my-0'} />
            <Col span={24} className={'flex justify-end '}>
              <Button
                htmlType={'submit'}
                type={'primary'}
                icon={<FontAwesomeIcon icon={faArrowRight} />}
                loading={status === Status.PENDING}
              >
                Continue
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    </Layout>
  );
}

export default SelectImagePage;
