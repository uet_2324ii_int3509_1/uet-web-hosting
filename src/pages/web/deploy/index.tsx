import { Button, Card, Col, Collapse, Divider, Form, Input, Layout, Row, Select, Typography } from 'antd';
import { Runtime } from '~/enum/app.enum.ts';
import NewFieldItem from '~/components/Services/Web/NewFieldItem';
import { faArrowRight, faCircleMinus, faCirclePlus, faCube } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function WebDeployPage() {
  const [form] = Form.useForm();

  async function onDeployImage(payload: any) {
    try {
      console.log({ payload });
    } catch (error) {}
  }

  return (
    <Layout
      style={{
        maxWidth: 992,
        margin: '0 auto'
      }}
    >
      <div className={'mb-12'}>
        <Typography.Title level={3}>Create a new Web Service</Typography.Title>
        <Typography.Text type={'secondary'}>
          Deploy your web service from an image hosted on a public or private registry.
        </Typography.Text>
      </div>
      <Card bordered={false}>
        <Form form={form} initialValues={{ env: [''] }} onFinish={onDeployImage}>
          <Row gutter={[32, 24]}>
            <Col span={24}>
              <Typography.Title className={'mb-0'} level={5}>
                Deploy an image
              </Typography.Title>
            </Col>
            <Col span={8}>
              <NewFieldItem title={'Service name'} description={'The service name for your external image.'} />
            </Col>
            <Col span={16}>
              <Form.Item name={'name'}>
                <Input placeholder={'Service name...'} />
              </Form.Item>
            </Col>
            <Col span={8}>
              <NewFieldItem title={'Image URL'} description={'The image URL for your external image.'} />
            </Col>
            <Col span={16}>
              <Form.Item name={'registryUrl'}>
                <Input prefix={<FontAwesomeIcon icon={faCube} />} placeholder={'docker.io/library/nginx:latest'} />
              </Form.Item>
            </Col>
            <Divider className={'my-0'} />
            <Collapse
              className={'w-full'}
              defaultActiveKey={[]}
              ghost
              items={[
                {
                  key: 'credentials',
                  label: 'Credentials (Optional)',
                  children: (
                    <Row gutter={[32, 24]}>
                      <Col span={8}>
                        <NewFieldItem
                          title={'Docker hub ID'}
                          isOptional
                          description={'Username for your Docker account that lets you access Docker products.'}
                        />
                      </Col>
                      <Col span={16}>
                        <Form.Item name={'imageUrl'}>
                          <Input placeholder={'Docker hub ID...'} />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <NewFieldItem title={'Runtime'} description={'The runtime for your web service.'} />
                      </Col>
                      <Col span={16}>
                        <Form.Item name={'runtime'} rules={[{ required: true, message: 'Please choose the runtime' }]}>
                          <Select
                            options={[
                              {
                                label: 'NodeJS',
                                value: Runtime.NODE
                              },
                              {
                                label: 'Javascript',
                                value: Runtime.SPA
                              }
                            ]}
                          />
                        </Form.Item>
                      </Col>
                      <Col span={8}>
                        <NewFieldItem
                          isOptional
                          title={'Environment Variables'}
                          description={
                            'Set environment-specific config and secrets (such as API keys), then read those values from your code. '
                          }
                        />
                      </Col>
                      <Col span={16}>
                        <Form.List name='env'>
                          {(fields, { add, remove }) => (
                            <>
                              {fields.map(({ key, name }, index) => {
                                return (
                                  <div key={key} className={'w-full flex items-center gap-6'}>
                                    <Form.Item name={[name, 'key']} className={'flex-grow-1'}>
                                      <Input placeholder='Key' />
                                    </Form.Item>
                                    <Form.Item name={[name, 'value']} className={'flex-grow-1'}>
                                      <Input placeholder='Value' />
                                    </Form.Item>
                                    <Form.Item>
                                      {fields.length - 1 === index ? (
                                        <Button
                                          shape={'circle'}
                                          type={'text'}
                                          onClick={() => {
                                            add();
                                          }}
                                        >
                                          <FontAwesomeIcon icon={faCirclePlus} size={'xl'} />
                                        </Button>
                                      ) : (
                                        <Button
                                          shape={'circle'}
                                          type={'text'}
                                          onClick={() => {
                                            remove(name);
                                          }}
                                        >
                                          <FontAwesomeIcon icon={faCircleMinus} size={'xl'} />
                                        </Button>
                                      )}
                                    </Form.Item>
                                  </div>
                                );
                              })}
                            </>
                          )}
                        </Form.List>
                      </Col>
                    </Row>
                  )
                }
              ]}
            />

            <Divider className={'my-0'} />
            <Col span={24} className={'flex justify-end '}>
              <Button type={'primary'} icon={<FontAwesomeIcon icon={faArrowRight} />}>
                Continue
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    </Layout>
  );
}

export default WebDeployPage;
