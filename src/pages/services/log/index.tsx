import { Tabs } from 'antd';
import BuildLogsZone from '~/components/Services/Log/BuildLogsZone';
import DeployLogsZone from '~/components/Services/Log/DeployLogsZone';

export default function BuildLogsPage() {
  return (
    <Tabs
      defaultActiveKey='build'
      items={[
        {
          key: 'build',
          label: 'Build log',
          children: <BuildLogsZone />
        },
        {
          key: 'deploy',
          label: 'Runtime log',
          children: <DeployLogsZone />
        }
      ]}
    />
  );
}
