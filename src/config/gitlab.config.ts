export const VITE_GITLAB_CLIENT_ID = import.meta.env.VITE_GITLAB_CLIENT_ID;
export const GITLAB_REDIRECT_URI = 'http://localhost:5173/auth/gitlab/callback';

export const VITE_GITHUB_CLIENT_ID = import.meta.env.VITE_GITHUB_CLIENT_ID;

export const GITHUB_REDIRECT_URI = 'http://localhost:5173/auth/github/callback';
