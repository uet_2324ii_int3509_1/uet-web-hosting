import { Route, Routes } from 'react-router-dom';
import WebCreatePage from '~/pages/web/create';
import WebSelectRepoPage from '~/pages/web/select-repo';
import WebNewPage from '~/pages/web/new';
import WebDeployPage from '~/pages/web/deploy';
import SelectImagePage from '~/pages/web/select-image';
import WebNewImagePage from '~/pages/web/new-image';

export default function WebRoutes() {
  return (
    <Routes>
      <Route path={'/create'} element={<WebCreatePage />} />
      <Route path={'/select-repo'} element={<WebSelectRepoPage />} />
      <Route path={'/select-image'} element={<SelectImagePage />} />
      <Route path={'/new'} element={<WebNewPage />} />
      <Route path={'/new-image'} element={<WebNewImagePage />} />
      <Route path={'/deploy'} element={<WebDeployPage />} />
    </Routes>
  );
}
