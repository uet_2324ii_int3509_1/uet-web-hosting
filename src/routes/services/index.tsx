import { Navigate, Outlet, Route, Routes } from 'react-router-dom';
import ServicesLayout from '~/layout/ServicesLayout';
import ServicePage from '~/pages/services';
import BuildLogsPage from '~/pages/services/log';

export default function ServicesRoutes() {
  return (
    <Routes>
      <Route path={''} element={<ServicesLayout />}>
        <Route path={'/:id'} element={<Outlet />}>
          <Route path={''} element={<Navigate to={'deployments'} />} />
          <Route path={'deployments'} element={<ServicePage />} />
          <Route path={'log'} element={<BuildLogsPage />} />
          <Route path={'configurations'} element={<div>Configurations</div>} />
        </Route>
      </Route>
    </Routes>
  );
}
